package com.example.kakao.service;

import com.example.kakao.dto.GiveItemDTO;
import com.example.kakao.dto.GiveDTO;
import com.example.kakao.exception.TakeException;
import com.example.kakao.repository.GiveItemRepository;
import com.example.kakao.repository.GiveRepository;
import com.example.kakao.utils.TimeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class GiveTakeService {

    private final GiveRepository giveRepository;
    private final GiveItemRepository giveItemRepository;

    @Value("${service.take.expire:600}")
    private int expire;

    public GiveDTO search(String token, String roomId, int userId) throws TakeException {
        GiveDTO give = GiveDTO.builder().ownerId(userId).roomId(roomId).token(token).build();
        GiveDTO result = this.giveRepository.selectforSearch(give).orElseThrow(TakeException::invalidToken);
        result.setItems(this.giveItemRepository.selectList(result.getGiveId()).stream().filter(o->o.getOwnerId() != 0).collect(Collectors.toList()));
        return result;
    }

    @Transactional
    public GiveDTO save(String roomId, int userId, long totalPrice, int divideCount) {
        GiveDTO giveDTO = GiveDTO.builder()
                .roomId(roomId)
                .ownerId(userId)
                .totalPrice(totalPrice)
                .token(generateToken())
                .expireDt(TimeUtils.now() + this.expire)
                .build();

        int result = giveRepository.insert(giveDTO);
        if (result > 0) {
            List<GiveItemDTO> itemList = new ArrayList<>();

            divide(totalPrice, divideCount).stream()
                    .map(m -> GiveItemDTO.builder()
                            .giveId(giveDTO.getGiveId())
                            .price(m)
                            .build())
                    .forEach(itemList::add);

            giveItemRepository.insert(itemList);
            giveDTO.setItems(itemList);
        }
        return giveDTO;
    }

    public GiveItemDTO take(String token, String roomId, int userId) throws TakeException {
        GiveDTO give = this.giveRepository.selectByToken(token).orElseThrow(TakeException::noExistItem);
        if (!give.getRoomId().equals(roomId)) throw TakeException.invalidTake();
        if (give.getOwnerId() == userId) throw TakeException.invalidTake();
        if (give.getExpireDt() <= TimeUtils.now()) throw TakeException.expireTime();

        List<GiveItemDTO> itemAll = this.giveItemRepository.selectList(give.getGiveId());
        if (itemAll.stream().anyMatch(o -> o.getOwnerId() == userId)) {
            throw TakeException.alreadyTake();
        }

        // SELECT FOR UPDATE를 사용하면 X락이 발생할 뿐 아니라 기존에 이미 유저가 받았을 경우를 체크하기 어려우므로, 주인이 없는 레코드를 골라 업데이트해본다.
        List<GiveItemDTO> itemsList = itemAll.stream().filter(o -> o.getOwnerId() == 0).collect(Collectors.toList());
        for (GiveItemDTO item : itemsList) {
            int result = this.giveItemRepository.updateTake(GiveItemDTO.builder().ownerId(userId).itemId(item.getItemId()).build());
            if (result > 0) return item;
        }
        throw TakeException.soldOut();
    }

    private static List<Long> divide(final Long total, final int size) {
        List<Long> list = new ArrayList<>();
        Long remain = total;
        for (int i = 0; i < size; i++) {
            if (i+1 >= size) {
                list.add(remain);
                break;
            }
            Long price = (remain / 2);
            remain -= price;
            list.add(price);
        }
        return list;
    }

    private static String generateToken() {
        return RandomStringUtils.randomAlphabetic(3).toUpperCase();
    }
}
