package com.example.kakao.utils;

import java.time.Instant;

public class TimeUtils {
    public static long now() {
        return Instant.now().getEpochSecond();
    }
}
