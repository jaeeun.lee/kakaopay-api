package com.example.kakao.exception;

import com.example.kakao.controller.packet.ResponseType;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TakeException extends Exception {

    final int code;
    final String message;

    TakeException(ResponseType type) {
        this.code = type.getCode();
        this.message = type.getMessage();
    }

    public static TakeException invalidToken() { return new TakeException(ResponseType.INVALID_TOKEN); }

    public static TakeException noExistItem() {
        return new TakeException(ResponseType.NO_EXIST);
    }

    public static TakeException invalidTake() {
        return new TakeException(ResponseType.INVALID_ROOMID);
    }

    public static TakeException expireTime() {
        return new TakeException(ResponseType.EXPIRED);
    }

    public static TakeException soldOut() {
        return new TakeException(ResponseType.SOLD_OUT);
    }

    public static TakeException alreadyTake() { return new TakeException(ResponseType.ALREADY_TAKE);}
}
