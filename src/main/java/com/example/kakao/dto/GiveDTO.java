package com.example.kakao.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@ToString
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GiveDTO implements Serializable {

    @JsonProperty("give_id")
    private int giveId;

    @JsonProperty("room_id")
    private String roomId;

    @JsonProperty("token")
    private String token;

    @JsonProperty("owner_id")
    private int ownerId;

    @JsonProperty("total_price")
    private long totalPrice;

    @JsonProperty("take_price")
    private long takePrice;

    @JsonProperty("expire_dt")
    private long expireDt;

    @JsonProperty("insert_dt")
    private String insertDt;

    private List<GiveItemDTO> items;
}
