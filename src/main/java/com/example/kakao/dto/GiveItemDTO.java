package com.example.kakao.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GiveItemDTO implements Serializable {

    @JsonIgnore
    private int itemId;

    @JsonIgnore
    private int giveId;

    private long price;

    @JsonProperty("owner_id")
    private int ownerId;

}
