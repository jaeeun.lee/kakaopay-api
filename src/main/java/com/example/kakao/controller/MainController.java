package com.example.kakao.controller;

import com.example.kakao.controller.packet.BaseResponse;
import com.example.kakao.controller.packet.vo.GiveVO;
import com.example.kakao.controller.packet.ResponseType;
import com.example.kakao.controller.packet.vo.SearchVO;
import com.example.kakao.controller.packet.vo.TakeVO;
import com.example.kakao.dto.GiveDTO;
import com.example.kakao.dto.GiveItemDTO;
import com.example.kakao.exception.TakeException;
import com.example.kakao.service.GiveTakeService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class MainController {

    private final GiveTakeService giveTakeService;
    private final static int SUCCESS = 0;

    /**
     * 뿌리기 API
     * @param userId
     * @param roomId
     * @param request
     * @return
     */
    @PostMapping("give")
    @ResponseBody
    public BaseResponse give(
            @RequestHeader(value = "X-USER-ID") int userId,
            @RequestHeader(value = "X-ROOM-ID") String roomId,
            @RequestBody GiveVO.Request request
    ) {
        if (request.getPrice() == 0 || request.getDivideCount() == 0) {
            return new BaseResponse(ResponseType.INVALID_PARAM);
        }

        // DB 처리
        GiveDTO giveDTO = this.giveTakeService.save(roomId, userId, request.getPrice(), request.getDivideCount());

        GiveVO.Response response = GiveVO.Response.builder()
                .code(SUCCESS)
                .give(giveDTO)
                .build();

        return response;
    }

    /**
     * 받기 API
     * @param userId
     * @param roomId
     * @param request
     * @return
     */
    @PostMapping("take")
    @ResponseBody
    public BaseResponse take(@RequestHeader(value = "X-USER-ID") int userId,
                             @RequestHeader(value = "X-ROOM-ID") String roomId,
                             @RequestBody TakeVO.Request request) {
        try {
            GiveItemDTO item = this.giveTakeService.take(request.getToken(), roomId, userId);
            return TakeVO.Response.builder()
                    .price(item.getPrice())
                    .itemId(item.getItemId())
                    .build();
        } catch (TakeException tk) {
            return BaseResponse.builder()
                    .code(tk.getCode())
                    .message(tk.getMessage())
                    .build();
        }
    }

    /**
     * 조회 API
     * @param userId
     * @param roomId
     * @param request
     * @return
     */
    @PostMapping("search")
    @ResponseBody
    public BaseResponse search(@RequestHeader(value = "X-USER-ID") int userId,
                               @RequestHeader(value = "X-ROOM-ID") String roomId,
                               @RequestBody SearchVO.Request request) {
        try {
            GiveDTO give = this.giveTakeService.search(request.getToken(), roomId, userId);
            return SearchVO.Response.builder()
                    .give(give)
                    .build();
        } catch (TakeException tk) {
            return BaseResponse.builder()
                    .code(tk.getCode())
                    .message(tk.getMessage())
                    .build();
        }
    }
}

