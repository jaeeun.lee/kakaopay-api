package com.example.kakao.controller.packet.vo;

import com.example.kakao.controller.packet.BaseResponse;
import com.example.kakao.dto.GiveDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

public class SearchVO {
    @Data
    public static class Request {
        private String token;
    }

    @Data
    @SuperBuilder
    @EqualsAndHashCode(callSuper = true)
    public static class Response extends BaseResponse {

        @JsonProperty("give")
        private GiveDTO give;
    }
}
