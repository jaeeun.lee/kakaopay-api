package com.example.kakao.controller.packet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter @Setter
@SuperBuilder
public class BaseResponse {
    @Builder.Default
    private int code = 0;

    @Builder.Default
    private String message = "Success";

    @JsonIgnore
    private ResponseType responseType;

    public BaseResponse(ResponseType type) {
        this.code = type.getCode();
        this.message = type.getMessage();
    }
}
