package com.example.kakao.controller.packet.vo;

import com.example.kakao.controller.packet.BaseResponse;
import com.example.kakao.dto.GiveDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

public class GiveVO {
    @Data
    public static class Request {
        @JsonProperty("price")
        private long price;

        @JsonProperty("divide_count")
        private int divideCount;
    }

    @Data
    @SuperBuilder
    @EqualsAndHashCode(callSuper = true)
    public static class Response extends BaseResponse {

        @JsonProperty("give")
        private GiveDTO give;
    }
}
