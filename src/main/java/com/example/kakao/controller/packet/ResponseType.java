package com.example.kakao.controller.packet;

import lombok.Getter;

public enum ResponseType {
    INVALID_PARAM(20010, "Invalid param"),
    INVALID_TOKEN(20020, "Invalid token"),
    INVALID_REQUEST(20030, "Invalid request"),

    NO_EXIST(30000, "No exists take data"),
    INVALID_ROOMID(30010, "Invalid Room Id"),
    EXPIRED(30020, "Expired Already"),

    ALREADY_TAKE(40010, "Already take it"),
    SOLD_OUT(40030, "All item sold out already");

    @Getter
    private int code;
    @Getter
    private String message;

    ResponseType(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
