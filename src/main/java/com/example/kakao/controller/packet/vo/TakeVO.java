package com.example.kakao.controller.packet.vo;

import com.example.kakao.controller.packet.BaseResponse;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

public class TakeVO {

    @Data
    public static class Request {
        private String token;
    }

    @Data
    @SuperBuilder
    @EqualsAndHashCode(callSuper = true)
    public static class Response extends BaseResponse {
        private int itemId;
        private long price;
    }

}
