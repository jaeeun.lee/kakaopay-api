package com.example.kakao.repository;

import com.example.kakao.dto.GiveDTO;
import lombok.RequiredArgsConstructor;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class GiveRepository {

    private final SqlSessionTemplate sqlSessionTemplate;

    public Optional<GiveDTO> selectByToken(String token) {
        String queryId = "mapper.give.select-by-token";
        return Optional.ofNullable(this.sqlSessionTemplate.selectOne(queryId, token));
    }

    public Optional<GiveDTO> selectforSearch(GiveDTO dto) {
        String queryId = "mapper.give.select-for-search";
        return Optional.ofNullable(this.sqlSessionTemplate.selectOne(queryId, dto));
    }

    public int insert(GiveDTO dto) {
        String queryId = "mapper.give.insert";
        return this.sqlSessionTemplate.insert(queryId, dto);
    }

    public int updateSoldout(int giveId) {
        String queryId = "mapper.give.update-soldout";
        return this.sqlSessionTemplate.update(queryId, giveId);
    }
}
