package com.example.kakao.repository;

import com.example.kakao.dto.GiveItemDTO;
import lombok.RequiredArgsConstructor;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class GiveItemRepository {

    private final SqlSessionTemplate sqlSessionTemplate;

    public int insert(List<GiveItemDTO> paramlist) {
        if (paramlist != null && paramlist.size() > 0) {
            String queryId = "mapper.give-item.insert";
            return this.sqlSessionTemplate.insert(queryId, paramlist);
        }
        return 0;
    }

    public List<GiveItemDTO> selectList(int giveId) {
        String queryId = "mapper.give-item.selects-give";
        return this.sqlSessionTemplate.selectList(queryId, giveId);
    }

    public Optional<GiveItemDTO> selectOne(int itemId) {
        String queryId = "mapper.give-item.select";
        return Optional.ofNullable(this.sqlSessionTemplate.selectOne(queryId, itemId));
    }

    public Optional<GiveItemDTO> select(GiveItemDTO param) {
        String queryId = "mapper.give-item.select";
        return Optional.ofNullable(this.sqlSessionTemplate.selectOne(queryId, param));
    }

    public int updateTake(GiveItemDTO param) {
        String queryId = "mapper.give-item.update-take";
        return this.sqlSessionTemplate.update(queryId, param);
    }
}
