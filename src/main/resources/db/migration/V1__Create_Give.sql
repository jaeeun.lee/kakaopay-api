CREATE TABLE `give`
(
    `give_id`     INT(11)     NOT NULL AUTO_INCREMENT,
    `room_id`     VARCHAR(45) NOT NULL,
    `token`       VARCHAR(20) NOT NULL DEFAULT '',
    `owner_id`    INT(11)     NOT NULL,
    `total_price` BIGINT(20)  NOT NULL DEFAULT '0',
    `expire_dt`   BIGINT(20)  NOT NULL DEFAULT '0',
    `insert_dt`   DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_dt`   DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`give_id`),
    UNIQUE INDEX `unique_token` (`token` ASC)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
