CREATE TABLE `kakaopay`.`give_item`
(
    `item_id`   INT        NOT NULL AUTO_INCREMENT,
    `give_id`   INT(11)    NOT NULL,
    `price`     BIGINT(11) NOT NULL DEFAULT 0,
    `owner_id`  INT(11)    NULL,
    `insert_dt` DATETIME   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_dt` DATETIME   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`item_id`)
);
