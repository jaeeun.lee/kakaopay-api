package com.example.kakao;

import com.example.kakao.dto.GiveDTO;
import com.example.kakao.dto.GiveItemDTO;
import com.example.kakao.exception.TakeException;
import com.example.kakao.service.GiveTakeService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
class GiveTest {

    @Autowired
    private GiveTakeService giveTakeService;

    private static String roomId;
    private static int giveOwnerId;
    private static long totalPrice;
    private static int divideCount;

    @BeforeAll
    static void init() {
        roomId = RandomStringUtils.randomAlphanumeric(8);
        giveOwnerId = RandomUtils.nextInt(1000, 9999);
        totalPrice = RandomUtils.nextInt(1000, 99990);
        divideCount = RandomUtils.nextInt(1, 7);
    }

    @Test
    @DisplayName("재은이가 뿌린 돈은 정상적으로 분배가 되어야 한다. 모든 돈은 1원 이상이어야 하며, 의도한 건수와 총합은 일정해야 한다.")
    void 뿌리기테스트_정상동작() {
        long totalPrice = RandomUtils.nextInt(1000, 99999);
        int divideCount = RandomUtils.nextInt(1, 7);

        GiveDTO dto = this.giveTakeService.save(roomId, giveOwnerId, totalPrice, divideCount);

        assertEquals(divideCount, dto.getItems().size());
        assertEquals(dto.getTotalPrice(), dto.getItems().stream().map(GiveItemDTO::getPrice).mapToLong(o->o).sum());
        assertEquals(0, dto.getItems().stream().filter(o -> o.getPrice() < 1).count());
    }

    @Test
    @DisplayName("재은이가 N명에게 뿌린 돈은 3명이 받아갈 수 있어야 한다")
    void 받아가기테스트() throws TakeException {
        GiveDTO dto = this.giveTakeService.save(roomId, giveOwnerId, totalPrice, divideCount);
        int userId = RandomUtils.nextInt(1000, 9999);

        boolean exceptionCatched = false;
        for (int i=0; i < divideCount; i++) {
            try {
                GiveItemDTO item = giveTakeService.take(dto.getToken(), roomId, userId + i);
            } catch (TakeException tk) {
                exceptionCatched = true;
            }
        }
        assertFalse(exceptionCatched);
    }

    @Test
    @DisplayName("재은이가 N명에게 뿌린 돈은 N+1번째 사람이 받아가지 못한다.")
    void 받아가기초과_테스트() {
        GiveDTO dto = this.giveTakeService.save(roomId, giveOwnerId, totalPrice, divideCount);
        int userId = RandomUtils.nextInt(1000, 9999);

        boolean exceptionCatched = false;
        for (int i=0; i < divideCount+1; i++) {
            try {
                GiveItemDTO item = giveTakeService.take(dto.getToken(), roomId, userId + i);
            } catch (TakeException tk) {
                exceptionCatched = true;
            }
        }
        assertTrue(exceptionCatched);
    }

    @Test
    @DisplayName("재은이가 뿌린 돈은 재은이가 받아가지 못한다.")
    void 셀프받아가기_테스트() {
        GiveDTO dto = this.giveTakeService.save(roomId, giveOwnerId, totalPrice, divideCount);

        boolean exceptionCatched = false;
        try {
            GiveItemDTO item = giveTakeService.take(dto.getToken(), roomId, giveOwnerId);
        } catch (TakeException tk) {
            exceptionCatched = true;
        }
        assertTrue(exceptionCatched);
    }

    @Test
    @DisplayName("재은이가 뿌린 돈의 정보는 재은이가 볼 수 있다.")
    void 정보조회_테스트() {
        GiveDTO dto = this.giveTakeService.save(roomId, giveOwnerId, totalPrice, divideCount);

        // N-1명이 받아갔다고 가정하자.
        int userId = RandomUtils.nextInt(1000, 9999);
        for (int i=0; i < divideCount - 1; i++) {
            try {
                GiveItemDTO item = giveTakeService.take(dto.getToken(), roomId, userId + i);
            } catch (TakeException ignored) { }
        }

        Boolean exceptionCatched = false;
        try {
            GiveDTO give = giveTakeService.search(dto.getToken(), roomId, giveOwnerId);
            assertEquals(roomId, give.getRoomId());
            assertEquals(giveOwnerId, give.getOwnerId());
            assertThat(divideCount, greaterThan(give.getItems().size()));

        } catch (TakeException tk) {
            exceptionCatched = true;
        }
        assertFalse(exceptionCatched);
    }

    @Test
    @DisplayName("재은이가 뿌리지 않은 돈의 정보는 재은이가 볼 수 없다.")
    void 정보조회_타인정보_테스트() {
        GiveDTO dto = this.giveTakeService.save(roomId, giveOwnerId, totalPrice, divideCount);
        int userId = giveOwnerId + RandomUtils.nextInt(1000, 9999);

        Boolean exceptionCatched = false;
        try {
            GiveDTO give = giveTakeService.search(dto.getToken(), roomId, userId);
        } catch (TakeException tk) {
            exceptionCatched = true;
        }
        assertTrue(exceptionCatched);
    }
}
